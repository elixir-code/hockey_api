defmodule HockeyClient do
  @moduledoc """
  Documentation for HockeyClient.
  """

  @doc """
  Hello world.

  ## Examples

      iex> HockeyClient.hello()
      :world

  """
  def hello do
    :world
  end
end
