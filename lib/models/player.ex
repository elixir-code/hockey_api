defmodule Models.Player do
  defstruct [:fullName, :firstName, :lastName, :primaryNumber, :birthDate, :currentAge, :brithCity, :birthStateProvince, :birthCountry, :nationality, :height, :weight, :active,
     :shootsCatches, :rosterStatus, :currentTeam, :primaryPosition, alternateCaptain: false, captain: false, rookie: false,]
end
